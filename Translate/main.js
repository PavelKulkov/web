function check(){
	if(form.numberToConvert.value == "" || form.numberToConvert.value == "." || form.numberToConvert.value == "-"){
		document.getElementById("out").innerHTML ="Ошибка ввода данных";
	}
	else
	switch(form.startingScaleOfNotation.value){
		case '2': 
			regExr(/[^01.-]/);
		break;	 
		case '8':
			regExr(/[^0-7.-]/);
		break;
		case '10':
			regExr(/[^0-9.-]/);
		break;
		case '16':
			regExr(/[^0-9A-Fa-f.-]/);
	}
}
function regExr(reg){
	if(reg.test(form.number.value)){
		document.getElementById("out").innerHTML ="Ошибка ввода данных";
		document.form.reset();
	}
	else{
		convert();
	}
}
function convert(){
	var temp = parseInt(form.number.value,form.startingScaleOfNotation.value).toString(form.finalScaleOfNotation.value);
	document.getElementById("out").innerHTML = form.numberToConvert.value +'<sub>'+form.startingScaleOfNotation.value+'</sub>'+' = '+temp+'<sub>'+form.finalScaleOfNotation.value+'</sub>';
	document.form.reset();
}

