function main() {
	var name = {
		regExp:/\D/,
		fieldId:"name",
		outputError:"nameError",
		errorMessage:"Поля должны быть заполнены и не должны содержать чисел."
	}
	var surname = {
		regExp:/\D/,
		fieldId:"surname",
		outputError:"nameError",
		errorMessage:"Поля должны быть заполнены и не должны содержать чисел."
	}
	var login = {
		regExp:/[A-Za-z0-9._]{6,30}/,
		fieldId:"login",
		outputError:"loginError",
		errorMessage:"Логин может содеражть буквы латинского алфавита, цифры, точки нижние подчёркивания.Допустимое кол-во символов:6-30."
	}
	var email = {
		regExp:/^[\w.]+@[a-zA-Z0-9\-]+\.[a-zA-Z]{2,4}$/,
		fieldId:"email",
		outputError:"emailError",
		errorMessage:"Укажите полный адрес электронной почты (включая символ @)."
	}
	var yearOfBirth = {
		regExp:/(19|20)\d\d/,
		fieldId:"yearOfBirth",
		outputError:"dateError",
		errorMessage:"Неправильная дата. Укажите действительную дату своего рождения."
	}
	var phoneCode = {
		regExp:/[0-9]{3}/,
		fieldId:"phoneCode",
		outputError:"phoneError",
		errorMessage:"Неверный номер телефона"
	}
	var numberOfPhone = {
		regExp:/[0-9]{7}/,
		fieldId:"numberOfPhone",
		outputError:"phoneError",
		errorMessage:"Неверный номер телефона"
	}
	var dayOfBirth = {
		regExp:checkDayOfBirth(),
		fieldId:"dayOfBirth",
		outputError:"dateError",
		errorMessage:"Неправильная дата. Укажите действительную дату своего рождения."
	}

	checkInputData(name.regExp,name.fieldId,name.outputError,name.errorMessage);
	checkInputData(surname.regExp,surname.fieldId,surname.outputError,surname.errorMessage);
	checkInputData(login.regExp,login.fieldId,login.outputError,login.errorMessage);
	checkInputData(email.regExp,email.fieldId,email.outputError,email.errorMessage);
	checkInputData(yearOfBirth.regExp,yearOfBirth.fieldId,yearOfBirth.outputError,yearOfBirth.errorMessage);
	checkInputData(phoneCode.regExp,phoneCode.fieldId,phoneCode.outputError,phoneCode.errorMessage);
	checkInputData(numberOfPhone.regExp,numberOfPhone.fieldId,numberOfPhone.outputError,numberOfPhone.errorMessage);
	checkInputData(dayOfBirth.regExp,dayOfBirth.fieldId,dayOfBirth.outputError,dayOfBirth.errorMessage);
}
function checkInputData(regExp, checkField,errorField,message){
	if(!regExp.test(document.getElementById(checkField).value)){
		document.getElementById(errorField).innerHTML = message;
		document.getElementById(checkField).style.borderColor = "red";
	}
	else{
		document.getElementById(errorField).innerHTML = "";
		document.getElementById(checkField).style.borderColor = "black";
		return true;
	}
}
function checkDayOfBirth() {
	if (Information.month.value === "Апрель" || Information.month.value === "Июнь" || Information.month.value === "Сентябрь" ||
		Information.month.value === "Ноябрь") {
		var dayPattern = /(0[1-9]|[12][0-9]|3[0])/;
	}
	else {
		if (Information.month.value === "Февраль") {
			//Проверка на високосный год
			if (Information.yearOfBirth.value % 4 != 0 || (Information.yearOfBirth.value % 100 === 0 &&
				Information.yearOfBirth.value % 400 != 0))
				var dayPattern = /(0[1-9]|1[0-9]|2[0-8])/;
			else
				var dayPattern = /(0[1-9]|[12][0-9])/;
		}
		else {
			var dayPattern = /(0[1-9]|[12][0-9]|3[01])/;
		}
	}
	return dayPattern;
}